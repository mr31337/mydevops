#### Advanced monitoring
Вся настройка Prometheus будет производиться с испльзованием IaC из предыдущей практики. 

**1. Настройка метрик с loadbalancer**

Prometheus и все необходимые exporters уже установлены на сервера. В рамках практики будет настроен сбор метрик с: 
  - `nginx` базовые метрики ОС

Работы проводиться на **`devbox.slurm.io`**

Для обнаружения источников данных для Prometheus будет исользоваться механизм service discovery. Виртуальные машины разверныт в облаке Selectel, которое построено на базе `OpenStack`. Для настройки подключения Prometheus к OpenStack API необходимо перейти в каталог с ansible inventory, выполнив команду:

```bash 
cd ~/ansible-inventory
``` 

**Включаем сбор метрик**

Включаем сбор метрик с exporters, для этого в файле `slurm/hosts/host_vars/bastion/main.yml` меняем значения `false` -> `true` для следующих полей:

```yaml
prom_lb_blackbox_scraping: true
```

**Применяем изменения**

Для применения изменений на сервера необходимо запушить изменения в gitlab:

```bash
git commit -am 'Enable prometheus nginx blackbox monitoring'
git push
```

Проверяем результат, для этого идем в "Pipelines" форка проекта **ansible-inventory** и смотрим на результат.

* Импортируем Dashboard в Grafana

Открываем в браузере: `http://BASTION_IP:3000`. Нажимаем + слева вверху и выбираем import

> Чтобы узнать ip адрес bastion необходимо подключиться к нему и выполенить команду `curl ifconfig.io`

Вставляем ID: `7587`
Указываем в качестве source-prometheus: `Prometheus`

* Отключаем один из aplication servers

```bash
$ ssh puma-1
$ systemctl stop puma@xpaste
```
* Проверяем состояние в Grafana


!!! В результате видим, что приложение доступно, но по факту часть клиентов получают ошибку.

* Добавляем мониторинг application servers

```bash 
cd ~/ansible-inventory
``` 

**Включаем сбор метрик**

Включаем сбор метрик с exporters, для этого в файле `slurm/hosts/host_vars/bastion/main.yml` меняем значения `false` -> `true` для следующих полей:

```yaml
prom_backend_blackbox_scraping: true
```

**Применяем изменения**

Для применения изменений на сервера необходимо запушить изменения в gitlab:

```bash
git commit -am 'Enable prometheus backend blackbox monitoring'
git push
```

Проверяем результат, для этого идем в "Pipelines" форка проекта **ansible-inventory** и смотрим на результат.

* Проверяем состояние в Grafana

Вот теперь мы видим, что один из бэкэндов недоступен.

![puma_fail](img/puma_fail.png)

* Включаем обратно application server
```bash
$ ssh devbox.slurm.io
$ ssh puma-1
$ systemctl start puma@xpaste
```

**2. Мониторинг с ошибками**

**Подготовка**

В этой практике схема выполнения запросов меняется. Если раньше маршрут
запросов был `slapper` -> `nginx-balancer` -> `app`, то сейчас он будет выполняться по
новой схеме `slapper` -> `envoy` -> `nginx-balancer` -> `app`.

Для этого нам понадобятся новые запросы. Сгенерируем их.

```bash
$ ssh devbox.slurm.io
$ ssh bastion

# сдесь LB_IP - это IP-адрес load-balancer, а 127.0.0.1:9921 - это порт на котором работает envoy
$ ./generate-targets-for-slapper.pl --url http://<LB_IP>/paste-file --permalink_alternative_url http://127.0.0.1:9921 > targets-localhost
```

Убедимся, что `envoy` запущен
```bash
$ service envoy status
Redirecting to /bin/systemctl status envoy.service
● envoy.service - envoy
   Loaded: loaded (/etc/systemd/system/envoy.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2019-08-29 22:56:21 UTC; 22min ago
  Process: 9070 ExecReload=/usr/bin/kill -HUP $MAINPID (code=exited, status=0/SUCCESS)
 Main PID: 8964 (python)
   CGroup: /system.slice/envoy.service
           ├─8964 python /usr/local/bin/envoy-hot-restarter.py /usr/local/bin/envoy.start.sh
           └─9072 envoy --restart-epoch 0 --config-path /etc/envoy/envoy.yaml --parent-shutdown-time-s 30 --drain-time-s 20
```

Запустим нагрузку через envoy

```bash
# здесь LB_IP - это IP-адрес load-balancer
$ ./slapper -H "Host: <LB_IP>" -targets targets-localhost -maxY 500ms -rate 100
```

Запросы должны успешно выполняться.

**Заставим сервер генерировать ошибки (failure injection)**

Переходим на nginx-balancer

```bash
$ ssh devbox.slurm.io
$ ssh nginx-balancer
```

Добавим следующие строки в `/etc/nginx/conf.d/failure-injection.conf`

```
# этот код генерирует 500-ую ошибку с вероятностью в 5%.
# каждая ошибка будет сгенерирована с задержкой в диапазоне от 0 до 1ой секунды.

rewrite_by_lua_block {
  local rand = math.random
  if rand() < 0.05 then
    ngx.sleep(rand())
    return ngx.exit(500)
  end
}
```

Перезагрузим `nginx`

```bash
$ systemctl restart nginx
```

**Смотрим, что происходит в `slapper` и `grafana`**

В `slapper` должно появится некоторое количество 500ых ошибок. Причем их время
ответа остается в рамках около 1 секунды.

![slapper experience 500s](img/slapper-500s.png)

в `grafana` эти проблемы не видны:

![nginx_with_fails](img/nginx_with_fails.png)

**Создаем dashboard envoy с отображением ошибок**

Открываем в браузере: `http://BASTION_IP:3000`. Нажимаем + слева сверху и выбираем import

Вставляем ID: `7253`
Указываем в качестве DataSource: `Prometheus`

На графиках видно, процент запросов с **НЕ 50x** статусом

![gravana-envoy-fail-rate](img/gravana-envoy-fail-rate.png)

#### Добавим retry для envoy


Раскомментируем следующие строчки в `/etc/envoy/envoy.yaml`

```diff
-                 #timeout: 1s
+                 timeout: 1s
-                 #retry_policy:
+                 retry_policy:
-                    #retry_on: 5xx
+                    retry_on: 5xx
-                    #num_retries: 3
+                    num_retries: 3
-                    #per_try_timeout: 0.3s
+                    per_try_timeout: 0.3s
```

Перезагрузим `envoy`

```bash
$ systemctl reload envoy
Redirecting to /bin/systemctl reload envoy.service
```

Теперь в slapper:

![envoy_retry](img/envoy_retry.jpg)

Теперь давайте посмотрим в grafana:

![envoy_retry_grafana](img/envoy_retry_grafana.jpg)


Теперь по графикам видно, что все запросы выполняются без ошибок.
