### Базовая настройка Prometheus

Вся настройка Prometheus будет производиться с испльзованием IaC из предыдущей практики. 

**1. Настраиваем service discovery**



Prometheus и все необходимые exporters уже установлены на сервера. На данный момент Prometheus производит сбор только своих собственных метрик. В рамках первой практики будет настроен сбор метрик с: 
  - `nodeesporter` базовые метрики ОС
  - `gitlab-runner` метрики производительности gitlab-runner

Работы проводиться на **`devbox.slurm.io`**

Для обнаружения источников данных для Prometheus будет использоваться механизм service discovery. Виртуальные машины развернуты в облаке Selectel, которое построено на базе `OpenStack`. Для настройки подключения Prometheus к OpenStack API необходимо перейти в каталог с ansible inventory, выполнив команду:

```bash 
cd ~/ansible-inventory
``` 

**Шифруем пароль от OpenStack API**

Пароль совпадает с паролем от `devbox` и его можно посмотреть в (ЛК)[https://edu.slurm.io/notifications] и открываем на редактирование файл: `slurm/hosts/host_vars/bastion/main.yml`. В данном файле необходимо задать значения для подключения к `openstack API`, для этого задаем значения для следующих переменных:

Для этого шифруем пароль выполнив команду:

```bash
ansible-vault encrypt_string <MY_SELECTEL_PASSWORD>
```
Результат выполнеия команды необходимо подставить в поле `openstack_pass` в файл `slurm/hosts/host_vars/bastion/main.yml` и подставив данные своего пользователя и региона. В результате должно получиться, примерно так: 

```yaml
openstack_user: sXXXXXX
openstack_pass: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          35336239646561646436613139316364333562323961616137353133643266336232323730643830
          6263313030316337383236393238383631303562356439630a366134643130333161366561653565
          37323631316131613834313238636335303964623731633437373861663438656336653436616162
          3236303334396135340a653564333637323938653335313333373065376364336430316463613432
          3765
openstack_region: ru-X
```

**Включаем сбор метрик**

Включаем сбор метрик с exporters, для этого в файле `slurm/hosts/host_vars/bastion/main.yml` меняем значения `false` -> `true` для следующих полей:

```yaml
prom_node_exporter_scraping: true
prom_gitlab_runner_scraping: true
```
**Применяем изменения**

Для применения изменений на сервера необходимо запушить изменения в gitlab:

```bash
git commit -am 'Enable prometheus monitoring'
git push
```

Проверяем результат, для этого идем в "Pipelines" форка проекта **ansible-inventory** и смотрим на результат.

**2. Проверка работу Prometheus**

Открываем в UI Prometheus: `http://<BASTION_IP>:9090/targets`

> Чтобы узнать ip адрес bastion необходимо подключиться к нему и выполенить команду `curl ifconfig.io`

В результате вы должны увидить примерно следующую картину: 

![sd_targets](img/sd_targets.png)

Некоторые targets будут в состояние DOWN, сбор данных с них будет настроен позже.

**3. Включаем экспозицию в gitlab-runner**

gitlab-runner имеет встроенную поддержку prometheus. Работы проводиться на **`bastion`**. Для подключения необходимо выполнить команду:

```bash
ssh bastion
```

Теперь включим экспозицию метрик в gitlab-runner, для этого необходимо выполнить следующую команду:

```bash
sed -i '1 i\listen_address="0.0.0.0:9252"' /etc/gitlab-runner/config.toml
```

Эта команда запишет необходимую настройку в конфигурационный файл gitlab-runner. 

После этого, для применения настроек, необходимо перезапустить gitlab-runner выполнив команду:

```bash
systemctl restart gitlab-runner
```

**4. Настройка визуализации**

+ Создание data source

    * Переходим в UI grafana: `http://<BASTION_IP>:3000`

        > Чтобы узнать ip адрес bastion необходимо подключиться к нему и выполенить команду `curl ifconfig.io`
    
        Имя пользователя и пароль по умолчанию:
        ```
        user: admin
        password: admin
        ```
        Новый пароль ставим такой же, как и пароль для доступа у учетной записи студента.

    * Добавляем data source Prometheus:

        ```
        URL: http://127.0.0.1:9090
        ```

        Все остальное оставляем по умолчанию и сохраняем

        ![img/grafana_ds](img/grafana_ds.png)

        Нажимаем `Test & Save`

+ Создаем dashboard

Нажимаем + слева вверху и выбираем import

Вставляем ID node exporter dashboard: `1860`

Вставляем ID gitlab-runner dashboard: `10156`
