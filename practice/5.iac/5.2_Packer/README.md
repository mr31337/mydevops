# Внимание!

## !!!Всю практику выполняем на `devbox.slurm.io` под вашим пользователем `s00****`, предварительно зайдя на него по SSH!!!

### Packer

**Цель:** Создать два образа и на основе них запустить VDS нашим модулем. 

Packer создаёт вычислительные ресурсы для своей работы у провайдера, но за исключением сети. Для начала работы нужно создать сеть. Для экономии времени я скопировал манифест про сеть в папку terraform. 

0) *Cоздать сеть в вашем облаке для запуска worker серверов packer.*

```bash
cd ~/devops/practice/5.iac/5.2_Packer/terraform
ln ../../5.1_Terraform/server/secrets.tfvars ./secrets.tfvars
terraform init 
terraform apply -var-file=./secrets.tfvars
``` 
Как результат мы должны получить id нашей сети. 

```bash
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

network_id = 7451cc3d-e7fa-4fef-b8b5-7208cdff0df2
```
>Этот id надо запомнить и подставить в ~/devops/practice/5.iac/5.2_Packer/vars.json далее

1) *Подготовка переменных*
```bash
cd ~/devops/practice/5.iac/5.2_Packer/
cp vars.json.example vars.json
vim vars.json
```

Пример заполнения:
![](packer-vars.png)

2) *"Запечем" образ приложения и базы данных*
```bash
packer validate -var-file=vars.json packer.json
packer build -parallel=false -var-file=vars.json packer.json
```

>Процесс занимает несколько минут. В это время можно изучить структуру текущего каталога.
>В конце будет сообщение:
```bash
==> Builds finished. The artifacts of successful builds are:
--> app: An image was created: dc886005-c217-4e4a-bed1-84c4d7850011
--> db: An image was created: 7540628b-921f-4920-9218-69ca2cb51bb4
<<<<<<< HEAD
```
>Полученные id для образов нам потребуется далее. 
>На основании этих образов надо теперь запустить серверы. 


3) *создаем манифест для серверов*

В директории ~/devops/practice/5.iac/5.2_Packer/terraform лежит наш модуль create_server
Наше приложение xpaste в образе 'app' подключается к базе данных по адресу 192.168.0.100
Для этого в файле ~/devops/practice/5.iac/5.2_Packer/terraform/create_server/main.tf есть переменная для указания ip:

```bash
###################################
# Create port
###################################
resource "openstack_networking_port_v2" "port_1" {
  name       = "node-eth0"
  network_id = var.network_id

  fixed_ip {
    subnet_id = var.subnet_id
>>>    ip_address   = var.fixed_ip
  }
}
```
По аналогии с прошлым заданием в файле ~/devops/practice/5.iac/5.2_Packer/terrafrom/main.tf создать два сервера:

```bash
cd ~/devops/practice/5.iac/5.2_Packer/terraform
vim main.tf
```
Два раза вызываем наш модуль:

```bash
module "server_db" {
  source = "./create_server"
  network_id  = openstack_networking_network_v2.network_1.id
  subnet_id   = openstack_networking_subnet_v2.subnet_1.id

  image_id    = #указать id для DB image и APP соотсветвенно
  volume_type = var.volume_type
  az_zone     = var.az_zone
  flavor_id   = openstack_compute_flavor_v2.flavor-node.id
  key_pair_id = openstack_compute_keypair_v2.terraform_key.id
  fixed_ip    = "192.168.0.100"# 100 для DB, 101 для app.
}
module "server_app" {
... то же самое
}
```
4) *запускаем создание*
```bash
terraform init
terraform apply -var-file=./secrets.tfvars
```


5) *"Белый" ip для нашего сервера.*

Чтобы открыть наше приложение нам надо белый ip. Floating ip навешивается на порт, нам надо переменную openstack_networking_port_v2 передать из модуля в главный тред. Добавляем в ~/devops/practice/5.iac/5.2_Packer/terraform/create_server/output.tf вывод необходимого значения:

```
output "app_port_id" {
  value = openstack_networking_port_v2.port_1.id
}
```

После чего в ~/devops/practice/5.iac/5.2_Packer/terraform/main.tf создаем "белый" floating IP: 
```
###################################
# Create floating IP
###################################
resource "openstack_networking_floatingip_v2" "floatingip_1" {
  pool = "external-network"
}

###################################
# Link floating IP to internal IP
###################################
resource "openstack_networking_floatingip_associate_v2" "association_1" {
  port_id     = module.server_app.ИМЯ_ПЕРЕМЕННОЙ_ИЗ_МОДУЛЯ #мой пример app_port_id
  floating_ip = openstack_networking_floatingip_v2.floatingip_1.address
}
```
И чтобы этот ip увидеть в ~/devops/practice/5.iac/5.2_Packer/terraform/output.tf добавить:
```
output "server_external_ip" {
  value = openstack_networking_floatingip_v2.floatingip_1.address
}
```
Выполняем:
```
terraform apply -var-file=./secrets.tfvars

```

>И получаем ip нашего сервера:
```bash
Apply complete! Resources: 11 added, 0 changed, 0 destroyed.

Outputs:

network_id = 4e12e539-f0dd-4c19-8eb6-be243151d257
server_external_ip = 178.132.206.81
```
Заходим по этому ip и наблюдаем что миграция базы данных еще не прошла:
![](dbmigration.png)

Через минуту миграция прошла и наше xpaste приложение работает:
![](xpaste.png)

Подсказка если не получается: https://gist.github.com/artemsre/f09a48e69f9ea716cd2d5fbd2039bb98
=======

Удаляем серверы и ресурсы:
```
terraform destroy -var-file=./secrets.tfvars

```
