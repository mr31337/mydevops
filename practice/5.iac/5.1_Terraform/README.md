# Внимание!

## !!!Всю практику выполняем на `devbox.slurm.io` под вашим пользователем `s00****`, предварительно зайдя на него по SSH!!!

### Terraform первый запуск

**Цель:** создать тестовый сервер через terraform, познакомиться с модулями.


0) Подготовка

на `devbox.slurm.io` скачаем локальную копию репозитория:
```bash
ssh s000001@devbox.slurm.io
git clone git@gitlab.slurm.io:devops/devops.git
cd devops/
```

1) **Скопируем и заполним sercrets.tfvars.example**
>Поле domain_name одинаковое для всех: 
>domain_name: 82113
```bash
cd ~/devops/practice/5.iac/5.1_Terraform/server
cp secrets.tfvars.example secrets.tfvars
vim secrets.tfvars
```
>Пример заполнения  
>**public_key** - публичный ключ с шел-бокса `cat ~/.ssh/id_rsa.pub`

![](secrets.png)

2) **Запуск terraform,создание сервера.** 

```bash
terraform init

terraform plan -var-file=./secrets.tfvars

terraform apply -var-file=./secrets.tfvars
...
Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes <<<<<<<<<<<<<<<<<<<
```

>В конце будет вывод переменной `server_external_ip`, содержащий IP-адрес созданного сервера, можно зайти на него по ssh `ssh -l root $(terraform output server_external_ip)`
>Так же сервер будет доступен в панели управления. 


3) **Задание на понимание структуры объекта "resource" терраформа**

Создайте второй сервер, создав файл `server2.tf`, нужно будет скопировать из main.tf и настроить ресурсы port, volume, instance
Запускаем после создания файла:
```
terraform apply -var-file=./secrets.tfvars
```
Далее удаляем файл и запускаем еще раз:
```
rm server2.tf
terraform apply -var-file=./secrets.tfvars
```

Если у вас возникли сложности, можно свериться с файлом https://gist.github.com/artemsre/33a6532a3b070a6562f1e0a9d91e0bad 

*Задания:*
* Настройте вывод внутреннего адреса сервера по аналогии с переменной server_external_ip, переменную можно посмотреть в terraform.tfstate (обратите внимание что вывод появится после повторного terraform apply...) (hint:openstack_compute_instance_v2._NAME_.access_ip_v4)
* Параметризуйте значение размера памяти во flavor'e и задайте ему значение по-умолчанию по аналогии с `hdd_size`
* Избавится от переменной public_key. Ключ напрямую читать из ~/.ssh/id_rsa.pub используя функцию https://www.terraform.io/docs/configuration/functions/file.html


Решения доступно в файле https://gist.github.com/artemsre/cde515f76ba9d11c5299a874601285aa

После параметризации памяти во flavor'е можно обратиться к переменной из shell CLI и отресайзить сервер.
```bash
TF_VAR_ram=2048 terraform apply -var-file=./secrets.tfvars
```


## Модули

Сейчас чтобы добавить второй сервер нужно будет скопировать много ресурсов - порт, вольюм, инстанс. Данные ресурсы можно вынести в отдельный модуль, чтобы переиспользовать.

Создадим каталог `create_server` и файлы манифестов модуля
```bash
cd ~/devops/practice/5.iac/5.1_Terraform/server/
mkdir create_server
touch create_server/main.tf create_server/vars.tf create_server/output.tf
```

Перенесем ресурсы:  
`resource "openstack_networking_port_v2" "port_1"`  
`resource "openstack_blockstorage_volume_v3" "volume_1"`  
`resource "openstack_compute_instance_v2" "instance_1"`  
из **~/devops/practice/5.iac/5.1_Terraform/server/main.tf** в **~/devops/practice/5.iac/5.1_Terraform/server/create_server/main.tf**.  

В основном треде **~/devops/practice/5.iac/5.1_Terraform/server/main.tf** следует их удалить.

У модуля другая область видимости для переменных, и он может оперировать только с ресурсами, которые сам создает, либо с переменными. Так у нас отсутствует сеть и другие ресурсы то мы должны получать их через аргументы модуля при его вызове. Давайте заполним ~/devops/practice/5.iac/5.1_Terraform/server/create_server/vars.tf

```terraform
variable "network_id" {}
variable "subnet_id" {}
variable "hdd_size" {
    default = 5
}
variable "image_id" {}
variable "volume_type" {}
variable "flavor_id" {}
variable "key_pair_id" {}
variable "az_zone" {}
```

Теперь нужно переименовать обращения к этим ресурсам/переменным (файл ~/devops/practice/5.iac/5.1_Terraform/server/create_server/main.tf)

```terraform
openstack_networking_network_v2.network_1.id - > var.network_id

openstack_networking_subnet_v2.subnet_1.id -> var.subnet_id

data.openstack_images_image_v2.ubuntu.id -> var.image_id

openstack_compute_flavor_v2.flavor-node.id -> var.flavor_id

openstack_compute_keypair_v2.terraform_key.id -> var.key_pair_id
```

В конец файле ~/devops/practice/5.iac/5.1_Terraform/server/main.tf можно добавить вызов модуля:
```terraform
module "server_1" {
  source = "./create_server"

  network_id = openstack_networking_network_v2.network_1.id
  subnet_id= openstack_networking_subnet_v2.subnet_1.id

  image_id = data.openstack_images_image_v2.ubuntu.id
  volume_type = var.volume_type
  az_zone = var.az_zone
  flavor_id = openstack_compute_flavor_v2.flavor-node.id
  key_pair_id = openstack_compute_keypair_v2.terraform_key.id
}
```

Для упрощения сейчас закомментируем следующие блоки ресурсов, т.к. у нас поменялась область видимости.

В файле ~/devops/practice/5.iac/5.1_Terraform/server/main.tf
```terraform
# resource "openstack_networking_floatingip_v2" "floatingip_1" { ...
# resource "openstack_networking_floatingip_associate_v2" "association_1" { ...
```  
В outputs.tf:
```terraform
# output "server_external_ip" { ...
````
После этого можно запустить терраформ:
```bash
cd ~/devops/practice/5.iac/5.1_Terraform/server
terraform init
terraform apply -var-file=./secrets.tfvars
```

Сервер будет пересоздан
```bash
Plan: 4 to add, 0 to change, 6 to destroy.
```

_В случае каких-то проблем можно свериться с https://gist.github.com/artemsre/346f76010f571bf92f1622ab73259f07

Сервер будет пересоздан, т.к. изменился идентификатор объекта instance. Был openstack_compute_instance_v2.instance_1 стал module.server_1.

![](2020-01-07-23-44-01.png)

Если вызвать модуль из main.tf еще раз, то можно создать второй сервер
```terraform
module "server_2" {
  source = "./create_server"

  network_id = openstack_networking_network_v2.network_1.id
  subnet_id= openstack_networking_subnet_v2.subnet_1.id

  image_id = data.openstack_images_image_v2.ubuntu.id
  volume_type = var.volume_type
  az_zone = var.az_zone
  flavor_id = openstack_compute_flavor_v2.flavor-node.id
  key_pair_id = openstack_compute_keypair_v2.terraform_key.id
}
```

Можно удалить отдельно ресурсы модудя через опцию **target**
```bash
terraform destroy -target module.server_2 -var-file=./secrets.tfvars
```

**Самостоятельное задание:**

> Перед выполнением удалите секцию **module "server_2"** из ~/devops/practice/5.iac/5.1_Terraform/server/main.tf

* Используя опцию count и модуль create_server, создать два сервера - подробнее https://www.terraform.io/docs/configuration/resources.html#count-multiple-resource-instances-by-count (hint: в модуль передать переменную count)
* Через output переменную выведите внутренний IP-адрес сервера, нужно сделать output переменную внутри модуля, важно - т.к. мы используем несколько ресурсов с count, то в выводе ресурсов будет присутствовать звездочка "*".
* Модуль может быть удаленным - вынести модуль в git-репозиторий (gitlab, github и т.д.) и подключить его с указанием имени бранча или тега - https://www.terraform.io/docs/modules/sources.html#github

Подсказки - https://gist.github.com/artemsre/b0e25469fbde7e28f282d64a3141d378

## Подготовка к Packer.

Удалим сервера и другие объекты, т.к. дальше будет использоваться пакер.
```bash
terraform destroy -var-file=./secrets.tfvars
```
