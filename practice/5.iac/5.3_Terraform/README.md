# Terraform Tips and Trics

Цель: Познакомится с приемами и методами работы terraform

##Переменные окружения

При ежедневном использовании не указывают постоянно путь до secrets.tfvars . Указанные переменные обычно передаются в среду выполнения через CI/СD.

В нашем случае это должно быть:
![](export.png) 

Для локальной разработки есть вот такая хитрость. Можно сделать файл который автоматически подгружается terraform
```bash
cd ~/devops/practice/5.iac/5.3_Terraform/server/
cp ~/devops/practice/5.iac/5.1_Terraform/server/secrets.tfvars .auto.tfvars
terraform init
terraform plan
```

## Debug

Иногда случается так что из ошибки не понятно что именно не так сделано. Предлагаю "испортить" region переменную из первого урока.

```bash
cd ~/devops/practice/5.iac/5.3_Terraform/server/
vim .auto.tfvars # region значение сделать любое
terraform apply
```
Наблюдаем ошибку:
```bash
Error: Error creating OpenStack image client: No suitable endpoint could be found in the service catalog.

```
Не совсем понятно в чем мы именно ошиблись, чтобы понять суть проблемы надо можно запустить terrafrom в debug режиме:
```bash
TF_LOG=DEBUG terraform apply
``` 
И логически проследив выполнение понятно что оно падает после выбора региона:
```bash
2020-08-16T15:07:34.850+0300 [DEBUG] plugin.terraform-provider-openstack_v1.30.0_x4: 2020/08/16 15:07:34 [DEBUG] Unlocked "auth"
2020-08-16T15:07:34.850+0300 [DEBUG] plugin.terraform-provider-openstack_v1.30.0_x4: 2020/08/16 15:07:34 [DEBUG] OpenStack Region is: sdfsdf
Error: Error creating OpenStack image client: No suitable endpoint could be found in the service catalog.
```
Чиним значение переменной region в файле ~/devops/practice/5.iac/5.3_Terraform/server/.auto.tfvars
И запускаем создание сервера:
```bash
cd ~/devops/practice/5.iac/5.3_Terraform/server/
terraform apply
```
Сервер создан. 

## State file and import
Как мы уже обсуждали terraform хранить информацию о созданных объектах в своем state файле. Cмоделируем ситуацию что state файл terraform был поврежден для этого из него удалим resourse instance_1:
```bash
vim terraform.tfstate
``` 
Удаляем блок из json:
```json
    {
      "mode": "managed",
      "type": "openstack_compute_instance_v2",
      "name": "instance_1",
	....
    },
```
Запускаем terraform и смотрим к чему это привело:
```bash
terraform plan
```
Как видно из вывода terraform заново нам собирается создать наш сервер который у нас уже есть. Но для каждого ресурса есть возможность импортировать уже существующий ресурс. Заходим в вашу облачную панель https://*.selvpc.ru переходим в раздел с серверами и копируем id сервера:

![](panelid.png)

Подставляем этот id в команду:
```bash
terraform import openstack_compute_instance_v2.instance_1 630cb065-f721-42d6-9686-840f902961d4
```

>В ответ получаем
```
Import successful!
```
И проверяем статус нашего манифеста:

```bash
terraform apply
```
К сожалению импорт ресурса openstack_compute_instance_v2 совсем не совершенен и нам после импорта все равно придется пересоздать инстанс. Соглашаеся и пересоздаем. 
 
## Sensitive information
Обратите внимание что после каждого запуска terraform apply в выводе присутствует:
```bash
Outputs:

public_key = <sensitive>
```
Этот вывод согласно нашему файлу ~/devops/practice/5.iac/5.3_Terraform/server/outputs.tf

```bash
output "public_key" {
  value = openstack_compute_keypair_v2.terraform_key.public_key
  sensitive = true
}
```

Любую выводимую переменную можно обозначить флагом sensitive, в этом случае она не будет отображаться на экране после выполнения манифеста. Это необходимо для оперирования секретами, но тем не менее все переменные храняться в state файле в открытом виде. 
Посмотреть ее можно командой:
```bash
terraform show
```


## Создание ресурса по условию
Terraform манифесты описываются на декларативном языке HCL, декларативная природа описания не позволяет нам пользоваться той же самой логикой что и в обычных языках программирования. 
В итоге у нас нет логического If в широком смысле слова, но есть небольшая хитрость. 
Используя свойство count можно создать 0 объектов. Для удобства описания используется тернарный if для переменных, который существует в terraform. 
Расскоментируем в файле ~/devops/practice/5.iac/5.3_Terraform/server/main.tf строку в resource openstack_compute_instance_v2
```
###################################
# Create Server
###################################
resource "openstack_compute_instance_v2" "instance_1" {
  count = (var.create_server != "" ? 1 : 0)
  name              = "node"
```

Ну и соответвенно у нас уже есть переменная create_server в ~/devops/practice/5.iac/5.3_Terraform/server/vars.tf и значение по умолчанию true. Можете  проверить:

```bash
cd ~/devops/practice/5.iac/5.3_Terraform/server/
cat vars.tf
terraform plan
```
Сделаем значение переменной "" (пустая строка).
```bash
echo 'create_server=""' >> .auto.tfvars
terraform plan
```
И убеждаемся что terraform собирается удалить наш instance_1

## Частый случай использования свойства count

 В файле vars описывается массив объектов. Предположим нам надо создать пользователей в системе тогда vars.tf у нас будет:
```bash
variable "users" {
  type    = list(string)
  default = ["mike","nicola","john","linux"]
}
```
И далее мы описывает ресурс 
```bash
resource "aws_iam_user" "create_users" {
  count=length(var.users)
  name = var.users[count.index]
}
```

Но есть ньюанс если мы удалим одного пользователя посередине списка:
```bash
default = ["mike","john","linux"]
```
То john и linux пользователи пересоздадутся. Так как для count терраформ считает уникальность в нашем случае по связке count.index + Name. 

## State и блокировки

>DEMO от преподавателя и обяснение принципов использования remote state. 

## Чего ждем от terraform 0.13

https://www.hashicorp.com/blog/announcing-the-terraform-0-13-beta/

10 августа вышел terrafrom 0.13 из значимых улучшений которые я ожидаю это возможность использовать "count" и for_each для модулей. 

То есть уже сейчас можно объявить переменную список:
```bash
variable "pg-databases" {
  type = map(object({
      size = string
      replica = bool
  }))
}
```
И дальше сделать цикл по своему модулю:
```bash
module "create_GCP_databases" {
  source   = "../../module/create_databases"
  for_each = var.pg-databases
 
  name = each.key
  size = each.value.size
  replica = each.value.replica
}

```

## Императивные возможности Pulumi и CDK

В виду декларативных ограничений языка HCL инженеры ищут возможности более гибко описывать логику инфраструктуры. И тут есть два варианта

### CDK Terraform

https://www.hashicorp.com/blog/cdk-for-terraform-enabling-python-and-typescript-support/


![](cdk.png)

![](cdk-python.png)

### Pulumi 
![](pulumi-python.png)

Удаляем серверы и ресурсы:
```
terraform destroy -var-file=./secrets.tfvars

```
